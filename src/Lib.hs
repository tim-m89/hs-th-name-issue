{-# LANGUAGE TemplateHaskell #-}

module Lib where

import Language.Haskell.TH.Quote
import Language.Haskell.TH.Lib
import Language.Haskell.TH.Syntax
import Foreign.C.String
import Foreign.Ptr

importGen = QuasiQuoter { quoteExp  = undefined
                        , quotePat  = undefined
                        , quoteType = undefined
                        , quoteDec  = importGenDec }

importGenDec :: String -> Q [Dec]
importGenDec s = do
                let sig1  = [t|CString -> IO ()|]
                sig2     <- [t|(FunPtr $sig1) -> ($sig1)|]
                name1    <- newName "makeFun"
                name2    <- newName "makeFun"
                dec1     <- return $ ForeignD $ ImportF CCall Safe "dynamic" name1 sig2
                dec2     <- return $ ForeignD $ ImportF CCall Safe "dynamic" name2 sig2
                return $ [dec1, dec2]

